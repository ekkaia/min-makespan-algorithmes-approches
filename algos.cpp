#include <random>

using namespace std;

/**
 * @brief Classe des algorithmes LSA, LPT et RMA
 */
class Algos {
public:
    Algos() = default;

    /**
     * @brief Algorithme LSA
     * 
     * @param D Un tableau d'entiers D correspondant aux durées des tâches
     * @param M Un tableau d'entiers M correspondant aux machines
     * @return int temps total de réalisation calculé
     */
    static int list_scheduling_algorithm(std::vector<int> D, std::vector<int> M) {
        for (int i : D) {
            M[std::distance(std::begin(M), std::min_element(std::begin(M), std::end(M)))] += i;
        }

        return Algos::max_element(M);
    }

    /**
     * @brief Algorithme LPT
     * 
     * @param D Un tableau d'entiers D correspondant aux durées des tâches
     * @param M Un tableau d'entiers M correspondant aux machines
     * @return int temps total de réalisation calculé
     */
    static int largest_processing_time(std::vector<int> D, std::vector<int> M) {
        sort(D.begin(), D.end(), greater<>());
        return list_scheduling_algorithm(D, M);
    }
    /**
     * @brief Algorithme RMA
     * 
     * @param D Un tableau d'entiers D correspondant aux durées des tâches
     * @param M Un tableau d'entiers M correspondant aux machines
     * @return int temps total de réalisation calculé
     */
    static int random_machine_assignment(std::vector<int> D, std::vector<int> M) {
        for (int i : D) {
            M[rand() % M.size()] += i;
        }

        return Algos::max_element(M);
    }

    /**
     * @brief Élément max d'un tableau
     * 
     * @param arr Un tableau d'entiers
     * @return int l'entier le plus grand du tableau
     */
    static int max_element(std::vector<int> arr) {
        return *std::max_element(arr.begin(), arr.end());
    }

    /**
     * @brief Somme des éléments d'un tableau
     * 
     * @param arr Un tableau d'entiers
     * @return int la somme des entiers du tableau
     */
    static int sum_elements(const std::vector<int>& arr) {
        int sum = 0;
        for (auto& x : arr) {
            sum += x;
        }
        return sum;
    }

    /**
     * @brief Somme des éléments d'un tableau
     * 
     * @param arr Un tableau de réels
     * @return float la somme des réels du tableau
     */
    static float sum_elements(const std::vector<float>& arr) {
        float sum = 0;
        for (auto& x : arr) {
            sum += x;
        }
        return sum;
    }
};