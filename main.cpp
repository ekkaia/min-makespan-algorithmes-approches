#include <iostream>
#include <cmath>
#include <vector>
#include <chrono>
#include <algorithm>
#include "instances.cpp"
#include "algos.cpp"
#include "instance_p.cpp"

using namespace std;

/**
 * @brief Générateur d'instances Ip
 * 
 * @param p Un entier pour la génération de la durée des tâches et du nombre de machines
 */
void p_instance_generation(int p) {
    InstanceP instanceP(p);

    float maxLowerLimit = Algos::max_element(instanceP.D);
    float averageLowerLimit = Algos::sum_elements(instanceP.D) / instanceP.M.size();
    float maxBetweenMaxAndAverage = std::max(maxLowerLimit, averageLowerLimit);

    float resultLSA = Algos::list_scheduling_algorithm(instanceP.D, instanceP.M);
    float ratioLSA = resultLSA / maxBetweenMaxAndAverage;

    float resultLPT = Algos::largest_processing_time(instanceP.D, instanceP.M);
    float ratioLPT = resultLPT / maxBetweenMaxAndAverage;

    float resultRMA = Algos::random_machine_assignment(instanceP.D, instanceP.M);
    float ratioRMA = resultRMA / maxBetweenMaxAndAverage;

    cout << "Borne inférieure 'maximum' = " << maxLowerLimit << endl;
    cout << "Borne inférieure 'moyenne' = " << averageLowerLimit << endl;
    cout << endl;
    cout << "Résultat LSA = " << resultLSA << endl;
    cout << "Ratio LSA = " << ratioLSA << endl;
    cout << endl;
    cout << "Résultat LPT = " << resultLPT << endl;
    cout << "Ratio LPT = " << ratioLPT << endl;
    cout << endl;
    cout << "Résultat RMA = " << resultRMA << endl;
    cout << "Ratio RMA = " << ratioRMA << endl;
}

/**
 * @brief Générateur des instance IR
 * 
 * @param m Nombre de machines
 * @param n Nombre de tâches
 * @param k Nombre d'instances IR
 * @param dmin Durée minimum d'une tâche
 * @param dmax Durée maximum d'une tâche
 */
void random_instances_generator(int m, int n, int k, int dmin, int dmax) {
    std::vector<Instance> instanceList;

    for (int i = 0; i < k; i++) {
        Instance newInstance(m, n, dmin, dmax);
        instanceList.push_back(newInstance);
    }

    std::vector<float> ratioLSAList;
    std::vector<float> ratioLPTList;
    std::vector<float> ratioRMAList;

    for (Instance i : instanceList) {
        float maxLowerLimit = Algos::max_element(i.D);
        float averageLowerLimit = Algos::sum_elements(i.D) / i.M.size();
        float maxBetweenMaxAndAverage = std::max(maxLowerLimit, averageLowerLimit);

        float resultLSA = Algos::list_scheduling_algorithm(i.D, i.M);
        ratioLSAList.push_back(resultLSA / maxBetweenMaxAndAverage);

        float resultLPT = Algos::largest_processing_time(i.D, i.M);
        ratioLPTList.push_back(resultLPT / maxBetweenMaxAndAverage);

        float resultRMA = Algos::random_machine_assignment(i.D, i.M);
        ratioRMAList.push_back(resultRMA / maxBetweenMaxAndAverage);
    }

    float averageRatioLSA = Algos::sum_elements(ratioLSAList) / k;
    float averageRatioLPT = Algos::sum_elements(ratioLPTList) / k;
    float averageRatioRMA = Algos::sum_elements(ratioRMAList) / k;

    cout << "Ratio moyen LSA = " << averageRatioLSA << endl;
    cout << "Ratio moyen LPT = " << averageRatioLPT << endl;
    cout << "Ratio moyen RMA = " << averageRatioRMA << endl;
}

int main() {
    srand(time(nullptr));
    int choice;
    cout << "Choisissez entre deux modes de saisie des instances d'entrée \n 1. Génération d'un instance de type Ip \n 2. Génération aléatoire de plusieurs instances" << endl;
    cin >> choice;

    if (choice == 1) {
        int p;
        cout << "Entrez un entier 'p' :" << endl;
        cin >> p;
        cout << "p = " << p << endl;
        p_instance_generation(p);
    } else if (choice == 2) {
        int m, n, k, dmin, dmax;
        cout << "Entrez 5 entiers pour les valeurs de 'm', 'n', 'k', 'dmin', 'dmax' :" << endl;
        cin >> m;
        cout << " m = " << m << endl;
        cin >> n;
        cout << " n = " << n << endl;
        cin >> k;
        cout << " k = " << k << endl;
        cin >> dmin;
        cout << " dmin = " << dmin << endl;
        cin >> dmax;
        cout << " dmax = " << dmax << endl;
        random_instances_generator(m, n, k, dmin, dmax);
    } else if (choice == 0) {
        // Pour les tests uniquement
        std::vector<int> values = {1,2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50,55,60,70,80,90,100,120,140,160,180,200,300};

        for (int p : values) {
            cout << "VALEUR DE TEST : " << p << endl;
            p_instance_generation(p);
        }
    } else {
        cout << "Erreur !" << endl;
    }

    return 0;
}


