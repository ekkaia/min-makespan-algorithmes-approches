## Build & execute

```
g++ main.cpp -std=c++17 -o min_makespan.exe
./min_makespan.exe
``` 

You might need to allow execution on the output file after compiling :
```
chmod +x min_makespan.exe
```