#include <iostream>
#include <random>

using namespace std;

/**
 * @brief Classe pour la génération des instance IR
 */
class Instance {
public:
    std::vector<int> D;
    std::vector<int> M;

    /**
     * @brief Construit une nouvelle instance IR
     * 
     * @param m Le nombre de machines
     * @param n Le nombre de tâches
     * @param dmin La durée minimum d'une tâche
     * @param dmax La durée maximum d'une tâche
     */
    Instance(int m, int n, int dmin, int dmax) {
        this->fillM(m);
        this->fillD(n, dmin, dmax);
    }

    /**
     * @brief Affiche le contenu du tableau D
     */
    void displayD() {
        for (int i : this->D) {
            cout << i << " ; ";
        }
    }

    /**
     * @brief Affiche le contenu du tableau M
     */
    void displayM() {
        for (int i : this->M) {
            cout << i << " ; ";
        }
    }

private:
    /**
     * @brief Remplis le tableau des machines de 0
     * 
     * @param m Nombre de machines
     */
    void fillM(int m) {
        for (int i = 0; i < m; i++) {
            this->M.push_back(0);
        }
    }

    /**
     * @brief Remplis le tableau des durées des tâches
     * 
     * @param n Nombre de tâches
     * @param dmin Durée minimum d'une tâche
     * @param dmax Durée maximum d'une tâche
     */
    void fillD(int n, int dmin, int dmax) {
        for (int i = 0; i < n; i++) {
            int duration = Instance::duration(dmin, dmax);
            this->D.push_back(duration);
        }
    }

    /**
     * @brief Calcule la durée d'une tâche
     * 
     * @param dmin Durée minimum d'une tâche
     * @param dmax Durée maximum d'une tâche
     * @return int durée calculée de la tâche
     */
    static int duration(int dmin, int dmax) {
        return rand() % (dmax - dmin + 1) + dmin;
    }
};