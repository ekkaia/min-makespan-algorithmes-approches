#include <iostream>
#include <random>

using namespace std;

/**
 * @brief Classe pour la génération des instance Ip
 */
class InstanceP {
public:
    std::vector<int> D;
    std::vector<int> M;
    int p;

    /**
     * @brief Construit une nouvelle instance Ip
     * 
     * @param p Un entier pour la génération de la durée des tâches et du nombre de machines
     */
    InstanceP(int p) : p(p) {
        this->fillM();
        this->fillD();
    }

    /**
     * @brief Affiche le contenu du tableau D
     */
    void displayD() {
        cout << "Tableau D :" << endl;
        for (int i : this->D) {
            cout << i << " ; ";
        }
        cout << endl;
    }

    /**
     * @brief Affiche le contenu du tableau M
     */
    void displayM() {
        cout << "Tableau M :" << endl;
        for (int i : this->M) {
            cout << i << " ; ";
        }
        cout << endl;
    }

private:
    /**
     * @brief Remplis le tableau des machines de 0
     */
    void fillM() {
        for (int i = 0; i < 2 * this->p; i++) {
            this->M.push_back(0);
        }
    }

    /**
     * @brief Remplis le tableau des durées des tâches
     */
    void fillD() {
        for (int i = 0; i < 4 * this->p; i++) {
            this->D.push_back(1);
        }

        for (int i = 0; i < (2 * pow(this->p, 2) - 2 * this->p); i++) {
            this->D.push_back(2);
        }

        this->D.push_back(2 * this->p);
    }
};